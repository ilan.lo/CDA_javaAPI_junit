package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import org.springframework.web.client.RestClient;

import java.sql.SQLException;

@SpringBootTest
class PersonTests {
    @Test
    void testRestConnect() {
        try{
            RestClient restClient = RestClient.create();
            Assertions.assertInstanceOf(RestClient.class, restClient);

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    @Test
    void testPersonValuesForGetOne() {
        try{
            Person person =  PersonDAO.getOne(1);
            assert(Integer.parseInt(person.getAnneeNaissance()) < 2024);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
