package com.cesi.cda.courspoo.Controller.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseController {

    public static final String BASE_URL = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1";

    public static DatabaseController INSTANCE = null;
    private static Connection connection = null;

    public static Connection getInstance() throws SQLException {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseController();
        }
        return connection;
    }

    private DatabaseController() throws SQLException {
        connection = DriverManager.getConnection("jdbc:h2:mem:dev-base", "romain", "romain");
    }

    public void close() throws SQLException {
        connection.close();
        connection = null;
        INSTANCE = null;
    }
}