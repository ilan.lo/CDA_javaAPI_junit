package com.cesi.cda.courspoo.Business;

import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Meet.MeetDAO;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public abstract class MatchBusiness {
    public static List<Match> getAll() throws SQLException {
        ArrayList<Match> listeToReturn = new ArrayList<Match>();

        ArrayList<Meet> meets = MeetDAO.getAll();
        for(Meet meet : meets) {
            Person winner = PersonDAO.getOne(meet.gagnant);
            Person loser = PersonDAO.getOne(meet.perdant);
            int sumAge = getAge(winner.getAnneeNaissance()) + getAge(loser.getAnneeNaissance());
            listeToReturn.add(new Match(meet.id, winner.getNomPrenom(), loser.getNomPrenom(), meet.lieu, meet.annee, sumAge));
        }
        return listeToReturn;
    }
    public static Match getOne(int id) throws SQLException {
        Meet meet = MeetDAO.getOne(id);
        Person winner = PersonDAO.getOne(meet.gagnant);
        Person loser = PersonDAO.getOne(meet.perdant);
        int sumAge = getAge(winner.getAnneeNaissance()) + getAge(loser.getAnneeNaissance());
        return new Match(meet.id, winner.getNomPrenom(), loser.getNomPrenom(), meet.lieu, meet.annee, sumAge);
    }

    /**
     *
     * @param match
     * @return
     * @throws SQLException
     */
    public static Match edit(Match match) throws SQLException{
        Meet meetInit = MeetDAO.getOne(match.id);
        meetInit.lieu = match.lieu;
        meetInit.annee = match.annee;
        Meet meet = MeetDAO.edit(meetInit);
        Person winner = PersonDAO.getOne(meet.gagnant);
        Person loser = PersonDAO.getOne(meet.perdant);
        int sumAge = getAge(winner.getAnneeNaissance()) + getAge(loser.getAnneeNaissance());
        return new Match(meet.id, match.getGagnant(), match.getPerdant(), meet.lieu, meet.annee, sumAge);
    }
    public static int delete(int id) throws SQLException{
        return MeetDAO.delete(id);
    }
    public static int getAge(String bornYear) {
        return 2024 - Integer.parseInt(bornYear);
    }
    public static int create(Match match, int idWinner, int idLoser) throws SQLException{

        Meet newMeet = new Meet(MeetDAO.getLast().id + 1, idWinner, idLoser, match.lieu, match.annee);
        return MeetDAO.create(newMeet);
    }
}
