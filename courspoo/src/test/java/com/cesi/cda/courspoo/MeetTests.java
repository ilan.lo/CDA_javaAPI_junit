package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.DAO.Meet.MeetDAO;
import com.cesi.cda.courspoo.DAO.Meet.Model.Meet;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import org.springframework.web.client.RestClient;

import java.sql.Connection;
import java.sql.SQLException;

//import java.sql.SQLException;
@SpringBootTest
public class MeetTests {
    @Test
    void testDataBaseConnect() {
        try{
            Connection connection = DatabaseController.getInstance();
            Assertions.assertInstanceOf(Connection.class, connection);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    @Test
    void testMeetValuesForGetOne() {
        try{
            Meet meet =  MeetDAO.getOne(1);
            assert(Integer.parseInt(meet.annee) < 2025);
            assert(meet.gagnant != meet.perdant);
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
