package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Business.MatchBusiness;
import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.Controller.Match.MatchController;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.h2.jdbc.JdbcSQLNonTransientException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import org.springframework.web.client.RestClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

@SpringBootTest
public class MatchCRUDTests {
    private Match match = new Match(38, "Rafael", "Roger", "DarkSideOfMoon", "1973", 5);;
    private Match matchGet;
    @Test
    void testCreate() {

        try {
            this.match.id = 38;
            this.match.annee = "1973";
            this.match.lieu = "DarkSideOfMoon";
            MatchBusiness.create(this.match, 1, 2);
            this.matchGet = MatchBusiness.getOne(38);
            // test if create + get
            Assertions.assertEquals(this.matchGet.annee, "1973");
            Assertions.assertEquals(this.matchGet.getLieu(), "DarkSideOfMoon");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    void testUpdate() {
        try {
            this.match.id = 37;
            this.match.annee = "2004";
            this.match.lieu = "Brest";
            MatchBusiness.edit(this.match);
            this.matchGet = MatchBusiness.getOne(37);

            Assertions.assertEquals(this.matchGet.annee, "2004");
            Assertions.assertEquals(this.matchGet.getLieu(), "Brest");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testDelete() {
        try {
            // test delete
            MatchBusiness.delete(36);

            Class<JdbcSQLNonTransientException> JdbcException = JdbcSQLNonTransientException.class;
            Assertions.assertThrows(JdbcException,() -> MatchBusiness.getOne(36));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
