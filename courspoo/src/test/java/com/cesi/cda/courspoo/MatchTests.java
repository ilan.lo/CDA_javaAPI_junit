package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.Business.MatchBusiness;
import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.cesi.cda.courspoo.DAO.Person.PersonDAO;
import org.springframework.web.client.RestClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

@SpringBootTest
public class MatchTests {
    private static <T> T get(URL url, Class<T> type) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(url, type);
    }
    private static JsonNode getJson(URL url) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(url);
    }
    @Test
    void testOnlineApi() {
        try{
                URL url = new URL("http://localhost:8080/api/test/?status=ACTIVE");
                JsonNode result = getJson(url);
                Assertions.assertEquals(String.valueOf(result), "1");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    @Test
    void testMatchValuesForGetOne() {
        try{
            Match match = MatchBusiness.getOne(1);
            Assertions.assertNotEquals(match.getGagnant(), match.getPerdant());
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void testGetAge() {
        for (int i = 0; i < 100; i++) {
            assert (MatchBusiness.getAge(String.valueOf(2024 - i)) == i);
        }
    }

    @Test
    void testIdNotHandled() {
        Class<SQLException> classSQLe = SQLException.class;
        Assertions.assertThrows(classSQLe,() -> MatchBusiness.getOne(-1));
    }

    @Test
    void testValuesFromAPIExposed() {
        try{
            Match match = MatchBusiness.getOne(1);
            Match matchFromUrl = new Match(1, "", "", "", "1900", 0);
            try {
                URL url = new URL("http://localhost:8080/api/getOne/1");
                Class<Match> responseType = Match.class;
                matchFromUrl = get(url, responseType);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            Assertions.assertEquals(match.id, matchFromUrl.id);
            Assertions.assertEquals(match.getSumAge(), matchFromUrl.getSumAge());
            Assertions.assertEquals(match.getLieu(), matchFromUrl.getLieu());
            Assertions.assertEquals(match.getAnnee(), matchFromUrl.getAnnee());
            Assertions.assertEquals(match.getGagnant(), matchFromUrl.getGagnant());
            Assertions.assertEquals(match.getPerdant(), matchFromUrl.getPerdant());
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
