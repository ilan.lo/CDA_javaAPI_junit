package com.cesi.cda.courspoo.DAO.Person;

import com.cesi.cda.courspoo.Controller.Database.DatabaseController;
import com.cesi.cda.courspoo.Controller.Match.Model.Match;
import com.cesi.cda.courspoo.DAO.Person.Model.Person;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClient;

import java.sql.SQLException;
import java.util.List;

@Repository
public class PersonDAO {
    public static Person getOne(int id) throws SQLException {
        RestClient restClient = RestClient.create();

        Person result = restClient.get()
                .uri(DatabaseController.BASE_URL +"/persons/"+ id)
                .retrieve()
                .body(Person.class);
        return result;
    }
    public static List<Person> getAll() throws SQLException {
        RestClient restClient = RestClient.create();
        //https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1/persons
        List<Person> results = restClient.get()
                .uri(DatabaseController.BASE_URL + "/persons")
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
        return results;
    }
}
